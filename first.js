//first
for (let i = 1; i < 101; i++) {
    if (i % 15 == 0) {
        console.log("FzzBuzz");
    }
    else if (i % 3 == 0) {
        console.log("Fizz");

    }
    else if (i % 5 == 0) {
        console.log("Buzz");
    }
    else {
        console.log(i);
    }
}






//second

function sort(input) {
    return input.reduce((acc, el) => {
        if (Array.isArray(el)) {
          acc = [...new Set([...acc, ...el])]
        }
        return acc
      }, [])
}

console.log(sort([[2], 23, "dance", true, [3, 5, 3]]));